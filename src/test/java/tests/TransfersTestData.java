package tests;

public interface TransfersTestData {
    String TRANSFER_RECIPENT = "Michal Jagoda";
    String TRANSFER_AMOUNT = "1000";
    String TRANSFER_TITLE = "Some transfer";
    String ACCOUNT_NUMBER = "1234567534";
    Integer LAST_PENDING_TRANSFER_ROW = 0;
}
