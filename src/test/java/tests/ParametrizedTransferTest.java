package tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.WebDriver;
import pages.MenuPage;
import pages.TransfersPage;

import java.util.concurrent.TimeUnit;

import static helpers.Configuration.getConfiguration;
import static helpers.Driver.initializeWebDriver;

public class ParametrizedTransferTest {
    private WebDriver driver;

    @BeforeEach
    public void setUp() {
        driver = initializeWebDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(getConfiguration().getSiteURL());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Some name with space", "!@##$%^&*()", "alert(\"Hello! I am an alert box!!\");"})
    public void fillAccountValidationTest(String parameter) {
        MenuPage menuPage = new MenuPage(driver);
        TransfersPage transfersPage = menuPage.selectTransfersPage()
                .fillAccountNumber(parameter);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }
}
