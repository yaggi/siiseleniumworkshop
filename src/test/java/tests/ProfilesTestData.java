package tests;

public interface ProfilesTestData {
    String NEW_NAME = "Zdzisław";
    String NEW_STREET = "Mickiewicza";
    String NEW_CITY = "Kutno";
    String NEW_POSTALCODE = "44-100";
    String NEW_EMAIL = "lalala@pl.com";
    String NEW_COUNTRY = "Sweden";
    String NEW_ADDITIONAL_MESSAGE = "Multiline message";
}
