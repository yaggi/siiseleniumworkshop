package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.MenuPage;
import pages.ProfilePage;

import static helpers.Configuration.getConfiguration;
import static helpers.Driver.initializeWebDriver;
import static org.junit.Assert.assertEquals;

public class ProfilesTest implements ProfilesTestData{
    private WebDriver driver;

    @Before
    public void setUp() {
        driver = initializeWebDriver();
        driver.get(getConfiguration().getSiteURL());
    }

    @Test
    public void updateUserProfileDataAndCheckItIsCorrectTest() {
        MenuPage menuPage = new MenuPage(driver);
        ProfilePage profilePage = menuPage.selectProfilePage()
            .updateName(NEW_NAME)
            .updateStreet(NEW_STREET)
            .updateCity(NEW_CITY)
            .updatePostalcode(NEW_POSTALCODE)
            .updateEmail(NEW_EMAIL)
            .updateCountry(NEW_COUNTRY)
            .selectGender(ProfilePage.GENDER_TYPE.FEMALE)
            .deselectNewsletter()
            .editAdditionalMessage(NEW_ADDITIONAL_MESSAGE)
            .sendUpdatedProfile();
        assertEquals("Profile name is not updated correctly",
                NEW_NAME, profilePage.getUpdatedPersonalData("Name"));
        assertEquals("Profile street is not updated correctly",
                NEW_STREET, profilePage.getUpdatedPersonalData("Street"));
        assertEquals("Profile city is not updated correctly",
                NEW_CITY, profilePage.getUpdatedPersonalData("City"));
        assertEquals("Profile postalcode is not updated correctly",
                NEW_POSTALCODE, profilePage.getUpdatedPersonalData("Postal code"));
        assertEquals("Profile email is not updated correctly",
                NEW_EMAIL, profilePage.getUpdatedPersonalData("Email"));
        assertEquals("Profile country is not updated correctly",
                NEW_COUNTRY, profilePage.getUpdatedPersonalData("Country"));
        assertEquals("Profile gender is not updated correctly",
                ProfilePage.GENDER_TYPE.FEMALE.toString().toLowerCase(),
                profilePage.getUpdatedPersonalData("Gender").toLowerCase());
        assertEquals("Profile note is not updated correctly",
                NEW_ADDITIONAL_MESSAGE, profilePage.getUpdatedPersonalData("Note"));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
