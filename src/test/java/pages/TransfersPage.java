package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransfersPage extends GenericPage {

    private static final String ACCOUNT_NUMBER_ID = "transfer-account";
    private static final String TRANSFER_TITLE_ID = "transfer-title";
    private static final String TRANSFER_RECIPENT_ID = "transfer-recipent";
    private static final String TRANSFER_AMOUNT_ID = "transfer-amount";
    private static final String SEND_TRANSFER_BUTTON_CSS = "input[value='Send']";
    private static final String PENDING_TRANSFER_TABLE_AMOUNT_VALUE_XPATH = "//*[@id=\"transfer-pending-table\"]/tbody/tr[1]/td[3]";
    private static final String RADIO_BUTTON_TRANSFER_FAST_CSS = "label[for='transfer-type-fast']";
    private static final String RADIO_BUTTON_TRANSFER_INSTANT_CSS = "label[for='transfer-type-instant']";
    private static final String RADIO_BUTTON_TRANSFER_STANDARD_CSS = "label[for='transfer-type-standard']";

    int LAST_TRANSFER_DATA_TABLE_TITLE_ID = 1;
    int LAST_TRANSFER_DATA_TABLE_AMOUNT_ID = 2;

    public enum TRANSFER_TYPE {
        STANDARD, FAST, INSTANT
    }

    public enum PENDING_TRANSFER_HEADING {
        DATE, TITLE, AMOUNT
    }

    public TransfersPage(WebDriver driver) {
        super(driver);
    }

    public TransfersPage fillAccountNumber(String accountNumber) {
        findElementByID(driver, ACCOUNT_NUMBER_ID).sendKeys(accountNumber);
        return this;
    }

    public TransfersPage fillTransferTitle(String transferTitle) {
        findElementByID(driver, TRANSFER_TITLE_ID).sendKeys(transferTitle);
        return this;
    }

    public TransfersPage fillTransferRecipent(String transferRecipent) {
        findElementByID(driver, TRANSFER_RECIPENT_ID).sendKeys(transferRecipent);
        return this;
    }

    public TransfersPage fillTransferAmount(String transferAmount) {
        waitUntilElementVisibleById(TRANSFER_AMOUNT_ID);
        findElementByID(driver, TRANSFER_AMOUNT_ID).sendKeys(transferAmount);
        return this;
    }

    public TransfersPage sendTransfer(){
        waitUntilElementVisibleByCSS(SEND_TRANSFER_BUTTON_CSS);
        findElementByCssSelector(driver, SEND_TRANSFER_BUTTON_CSS).click();
        return this;
    }

    public String getPendingTransferAmount() {
        return findElementByXpath(driver, PENDING_TRANSFER_TABLE_AMOUNT_VALUE_XPATH).getText();
    }

    public TransfersPage selectTransferType(TRANSFER_TYPE transfer_type) {
        switch (transfer_type) {
            case FAST:
                findElementByCssSelector(driver, RADIO_BUTTON_TRANSFER_FAST_CSS).click();
                return this;
            case STANDARD:
                findElementByCssSelector(driver, RADIO_BUTTON_TRANSFER_STANDARD_CSS).click();
                return this;
            case INSTANT:
                findElementByCssSelector(driver, RADIO_BUTTON_TRANSFER_INSTANT_CSS).click();
                return this;
        }
        return this;
    }

    public List<String> getAllTransfers() {
        List<WebElement> transfersWebElementsList = new ArrayList<WebElement>();
        List<String> transfersStringList = new ArrayList<String>();
        transfersWebElementsList.addAll(findElementByID(driver, "transfer-pending-table")
                .findElements(By.tagName("td")));
        for (WebElement element : transfersWebElementsList) {
            transfersStringList.add(element.getText());
        }
        return transfersStringList;
    }

    public Map<PENDING_TRANSFER_HEADING, String> getLatestTransferMap() {
        List<WebElement> pendingTransferWebElementList =
                new ArrayList<WebElement>();
        Map<PENDING_TRANSFER_HEADING, String> lastPendingTransferMap =
                new HashMap<PENDING_TRANSFER_HEADING, String>();
        pendingTransferWebElementList.
                addAll(findElementByID(driver, "transfer-pending-table")
                        .findElements(By.tagName("td")));
        if(pendingTransferWebElementList.size() > 0) {
            lastPendingTransferMap.
                    put(PENDING_TRANSFER_HEADING.DATE,
                            pendingTransferWebElementList.
                                    get(0).getText());
            lastPendingTransferMap.
                    put(PENDING_TRANSFER_HEADING.TITLE,
                            pendingTransferWebElementList.
                                    get(1).getText());
            lastPendingTransferMap.
                    put(PENDING_TRANSFER_HEADING.AMOUNT,
                            pendingTransferWebElementList.
                                    get(2).getText());
        }
        return lastPendingTransferMap;
    }

    public Map<Integer, Map<PENDING_TRANSFER_HEADING, String>> getAllPendingTransfersMap() {
        List<WebElement> pendingTransferWebElementList =
                new ArrayList<WebElement>();
        Map<Integer, Map<PENDING_TRANSFER_HEADING, String>> allPendingTransferMap =
                new HashMap<Integer, Map<PENDING_TRANSFER_HEADING, String>>();
        pendingTransferWebElementList.
                addAll(findElementByID(driver, "transfer-pending-table")
                        .findElements(By.tagName("td")));
        if(pendingTransferWebElementList.size() > 0) {
            for(int i=0; i < pendingTransferWebElementList.size(); i+=3) {
                Map<PENDING_TRANSFER_HEADING, String> rowTransferMap =
                        new HashMap<PENDING_TRANSFER_HEADING, String>();
                rowTransferMap.
                        put(PENDING_TRANSFER_HEADING.DATE,
                                pendingTransferWebElementList.
                                        get(i).getText());
                rowTransferMap.
                        put(PENDING_TRANSFER_HEADING.TITLE,
                                pendingTransferWebElementList.
                                        get(i+1).getText());
                rowTransferMap.
                        put(PENDING_TRANSFER_HEADING.AMOUNT,
                                pendingTransferWebElementList.
                                        get(i+2).getText());
                allPendingTransferMap.put(i/3, rowTransferMap);
            }
        }
        return allPendingTransferMap;
    }

    public String getPendingTransferData(Integer pendingTransferRow, PENDING_TRANSFER_HEADING pendingTransferColumn) {
        return getAllPendingTransfersMap().get(pendingTransferRow).get(pendingTransferColumn);
    }
}
