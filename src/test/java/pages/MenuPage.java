package pages;

import org.openqa.selenium.WebDriver;

public class MenuPage extends GenericPage{

    public MenuPage(WebDriver driver) {
        super(driver);
    }

    public TransfersPage selectTransfersPage() {
        findElementByLinkText(driver, "TRANSFER").click();
        return new TransfersPage(driver);
    }

    public ProfilePage selectProfilePage() {
        findElementByLinkText(driver, "PROFILE").click();
        return new ProfilePage(driver);
    }
}
