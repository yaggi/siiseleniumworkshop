package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfilePage extends GenericPage {

    @FindBy(id = "update-name")
    WebElement profileName;

    @FindBy(id = "update-street")
    WebElement profileStreet;

    @FindBy(id = "update-city")
    WebElement profileCity;

    @FindBy(id = "update-postalcode")
    WebElement profilePostalcode;

    @FindBy(id = "update-email")
    WebElement profileEmail;

    @FindBy(id = "update-country")
    WebElement updateCountry;

    @FindBy(css = "label[for='update-gender-male']")
    WebElement updateGenderMale;

    @FindBy(css = "label[for='update-gender-female']")
    WebElement updateGenderFemale;

    @FindBy(css = "label[for='update-gender-dont']")
    WebElement updateGenderDont;

    @FindBy(css = "label[for='update-newsletter']")
    WebElement updateNewsletter;

    @FindBy(id = "update-newsletter")
    WebElement updateNewsletterState;

    @FindBy(id = "update-additional")
    WebElement updateAdditionalMessage;

    @FindBy(css = "input[value='Save']")
    WebElement sendButton;

    public enum GENDER_TYPE {
        MALE, FEMALE, DONTASK
    }

    public ProfilePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public ProfilePage updateName(String newName) {
        profileName.sendKeys(newName);
        return this;
    }

    public ProfilePage updateCity(String newCity) {
        profileCity.sendKeys(newCity);
        return this;
    }

    public ProfilePage updateStreet(String newStreet) {
        profileStreet.sendKeys(newStreet);
        return this;
    }

    public ProfilePage updatePostalcode(String newPostalcode) {
        profilePostalcode.sendKeys(newPostalcode);
        return this;
    }

    public ProfilePage updateEmail(String newEmail) {
        profileEmail.sendKeys(newEmail);
        return this;
    }

    public ProfilePage updateCountry(String selectedCountry) {
        Select countryDropdown = new Select(updateCountry);
        countryDropdown.selectByVisibleText(selectedCountry);
        return this;
    }

    public ProfilePage selectGender(GENDER_TYPE gender_type) {
        switch (gender_type) {
            case MALE:
                updateGenderMale.click();
                return this;
            case FEMALE:
                updateGenderFemale.click();
                return this;
            case DONTASK:
                updateGenderDont.click();
                return this;
        }
        return this;
    }

    public ProfilePage deselectNewsletter(){
        if(updateNewsletterState.isSelected()) {
            updateNewsletter.click();
        }
        return this;
    }

    public ProfilePage selectCheckbox() {
        if(!updateNewsletterState.isSelected()) {
            updateNewsletter.click();
        }
        return this;
    }

    public ProfilePage editAdditionalMessage(String additionalMessage) {
        updateAdditionalMessage.sendKeys(additionalMessage);
        return this;
    }

    public ProfilePage sendUpdatedProfile() {
        fluentWaitForElementisDisplayed(sendButton);
        sendButton.click();
        return this;
    }

    public Map<String, String> getPersonalDataMap() {
        Map<String, String> personalDataMap = new HashMap<String, String>();
        List<WebElement> columnHeading = new ArrayList<WebElement>();
        List<WebElement> profileValues = new ArrayList<WebElement>();
        profileValues.addAll(findElementByID(driver, "personal-data-table")
                .findElements(By.tagName("td")));
        columnHeading.addAll(findElementByID(driver, "personal-data-table")
                .findElements(By.tagName("th")));
        for(int i = 0; i < columnHeading.size(); i++) {
            personalDataMap.put(columnHeading.get(i).getText(),
                    profileValues.get(i).getText());
        }
        return personalDataMap;
    }

    public String getUpdatedPersonalData(String personalData){
        return getPersonalDataMap().get(personalData);
    }
}
