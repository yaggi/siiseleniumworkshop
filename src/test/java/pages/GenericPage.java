package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class GenericPage {
    protected WebDriver driver;
    private static final Integer EXPLICIT_WAIT_TIMEOUT = 30;

    public GenericPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement findElementByLinkText(WebDriver driver, String linkText) {
        return driver.findElement(By.linkText(linkText));
    }

    public WebElement findElementByID(WebDriver driver, String id) {
        return driver.findElement(By.id(id));
    }

    public WebElement findElementByCssSelector(WebDriver driver, String cssSelector) {
        return driver.findElement(By.cssSelector(cssSelector));
    }

    public WebElement findElementByXpath(WebDriver driver, String xPath) {
        return driver.findElement(By.xpath(xPath));
    }

    public void waitUntilElementVisibleById(String idOfElement) {
        WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_TIMEOUT);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(idOfElement)));
    }

    public void waitUntilElementVisibleByCSS(String cssOfElement) {
        WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_TIMEOUT);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssOfElement)));
    }

    public void fluentWaitForElementisDisplayed(WebElement element) {
        new FluentWait<>(element)
                .withTimeout(10, TimeUnit.SECONDS)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .until(webElement -> webElement.isDisplayed());
    }
}
